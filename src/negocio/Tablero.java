package negocio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Tablero {

	
	public boolean[][] matriz;
	
	public Tablero(int i) {
		matriz = new boolean[i][i];
		
		//iniciar con random el tablero...
		int rand1 = (int) Math.floor(Math.random()*4);
		int rand2 = (int) Math.floor(Math.random()*4);
		int rand3 = (int) Math.floor(Math.random()*4);
		int rand4 = (int) Math.floor(Math.random()*4);
		int rand5 = (int) Math.floor(Math.random()*4);
		int rand6 = (int) Math.floor(Math.random()*4);
		int rand7 = (int) Math.floor(Math.random()*4);
		int rand8 = (int) Math.floor(Math.random()*4);
		matriz[rand1][rand2] = true;
		matriz[rand3][rand4] = true;
		matriz[rand5][rand6] = true;
		matriz[rand7][rand8] = true;

	}
	
	
	public void cambiarEstado(int fila, int columna) {
		matriz[fila][columna] = !matriz[fila][columna];
		
		try {
			matriz[fila][columna-1] = !matriz[fila][columna-1];
		} catch (Exception e) {
			
		}
		try {
			matriz[fila][columna+1] = !matriz[fila][columna+1];
		} catch (Exception e) {
			
		}
		try {
			matriz[fila+1][columna] = !matriz[fila+1][columna];
		} catch (Exception e) {
			
		}
		try {
			matriz[fila-1][columna] = !matriz[fila-1][columna];
			
		} catch (Exception e) {
			
		}
		

		
//		if(i == 0 && j == 0){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i+1][j] = !matriz[i+1][j];
//		}
//		if(i == 0 && j == 1){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i+1][j] = !matriz[i+1][j];
//		}
//		if(i == 0 && j == 2){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i+1][j] = !matriz[i+1][j];
//		}
//		if(i == 0 && j == 3){
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i+1][j] = !matriz[i+1][j];
//		}
//		if(i == 1 && j == 0){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i+1][j] = !matriz[i+1][j];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 1 && j == 1){
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i+1][j] = !matriz[i+1][j];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 1 && j == 2){
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i+1][j] = !matriz[i+1][j];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 1 && j == 3){
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i+1][j] = !matriz[i+1][j];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 2 && j == 0){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i+1][j] = !matriz[i+1][j];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 2 && j == 1){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i+1][j] = !matriz[i+1][j];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 2 && j == 2){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i+1][j] = !matriz[i+1][j];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 2 && j == 3){
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i+1][j] = !matriz[i+1][j];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 3 && j == 0){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 3 && j == 1){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 3 && j == 2){
//			matriz[i][j+1] = !matriz[i][j+1];
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
//		if(i == 3 && j == 3){
//			matriz[i][j-1] = !matriz[i][j-1];
//			matriz[i-1][j] = !matriz[i-1][j];
//		}
		
	}
	
	public boolean gameOver(){
		boolean flag = true;
	
		for(int i = 0; i<matriz.length; i++) {
			for(int j = 0; j<matriz[i].length;j++) {
				flag = flag && (matriz[i][j]==false);
			}
		}
		return flag;
		
	}
	
	public String mostrar() {
		String dibujo = "";
		
		for(int i = 0; i<matriz.length; i++) {
			for(int j = 0; j<matriz[i].length;j++) {
				if(matriz[i][j]) {
					if(j==3)
						dibujo+= "1 \n";
					else
						dibujo+= "1 ";
					
				}
				else {
					if(j==3)
						dibujo+= "0 \n";
					else
						dibujo+= "0 ";
				}
			}
		}
		return dibujo;
	}
	


	public static void main(String[] args) throws IOException {
		
		System.out.println ("EMPEZAMOS EL JUEGO!");
		System.out.println("----------------------- \n");
		Tablero tab = new Tablero(4);
		while(tab.gameOver()==false) {
			

			System.out.println(tab.mostrar());
			System.out.println("-----------------------");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


			System.out.println("Fila: ");
			String num1 = br.readLine(); 
			System.out.println("Columna: ");
			String num2 = br.readLine(); 


			int fila = Integer.parseInt(num1);

			int columna = Integer.parseInt(num2);

			System.out.println("-----------------------");
			tab.cambiarEstado(fila, columna);
			System.out.println(tab.mostrar());
			System.out.println("-----------------------");
			tab.gameOver();
		}
		System.out.println("ˇˇˇFELICITACIONES!!!");

	}
	
	
	
	
	
	
	

}