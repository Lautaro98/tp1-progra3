package interfaz;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JTable;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.border.MatteBorder;

import negocio.Tablero;

import java.awt.Button;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.Font;

public class MainForm {

	private JFrame frame;
	public Tablero tab;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//-----------------------------------------//
		
		//Arreglo de todos los botones
		ArrayList<JButton> arregloBotones = new ArrayList<JButton>();
		
		//Instancia el tablero 4x4 casillas
		tab = new Tablero(4);
		
		//JLabel lblNewLabel_1 = new JLabel("FELICITACIONES, HAS TERMINADO EL JUEGO!");
		

		JButton btn_0_0 = new JButton();
		btn_0_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(0, 0);
				actualizarTablero(arregloBotones);

			}
		});
		btn_0_0.setBounds(165, 65, 50, 50);
		frame.getContentPane().add(btn_0_0);
		arregloBotones.add(btn_0_0);

		JButton btn_0_1 = new JButton();
		btn_0_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(0, 1);
				actualizarTablero(arregloBotones);
			
			}
		});
		btn_0_1.setBounds(223, 65, 50, 50);
		frame.getContentPane().add(btn_0_1);
		arregloBotones.add(btn_0_1);

		JButton btn_0_2 = new JButton();
		btn_0_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(0, 2);
				actualizarTablero(arregloBotones);
				
			}
		});
		btn_0_2.setBounds(283, 65, 50, 50);
		frame.getContentPane().add(btn_0_2);
		arregloBotones.add(btn_0_2);

		JButton btn_0_3 = new JButton();
		btn_0_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(0, 3);
				actualizarTablero(arregloBotones);
			}
		});
		btn_0_3.setBounds(341, 65, 50, 50);
		frame.getContentPane().add(btn_0_3);
		arregloBotones.add(btn_0_3);

		JButton btn_1_0 = new JButton();
		btn_1_0.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				tab.cambiarEstado(1, 0);
				actualizarTablero(arregloBotones);
			}
		});
		btn_1_0.setBounds(165, 121, 50, 50);
		frame.getContentPane().add(btn_1_0);
		arregloBotones.add(btn_1_0);
		

		JButton btn_1_1 = new JButton();
		btn_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(1, 1);
				actualizarTablero(arregloBotones);
			}
		});
		btn_1_1.setBounds(223, 121, 50, 50);
		frame.getContentPane().add(btn_1_1);
		arregloBotones.add(btn_1_1);

		JButton btn_1_2 = new JButton();
		btn_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(1, 2);
				actualizarTablero(arregloBotones);
			}
		});
		btn_1_2.setBounds(283, 121, 50, 50);
		frame.getContentPane().add(btn_1_2);
		arregloBotones.add(btn_1_2);

		JButton btn_1_3 = new JButton();
		btn_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(1, 3);
				actualizarTablero(arregloBotones);
			}
		});
		btn_1_3.setBounds(341, 121, 50, 50);
		frame.getContentPane().add(btn_1_3);
		arregloBotones.add(btn_1_3);

		JButton btn_2_0 = new JButton();
		btn_2_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(2, 0);
				actualizarTablero(arregloBotones);
			}
		});
		btn_2_0.setBounds(165, 178, 50, 50);
		frame.getContentPane().add(btn_2_0);
		arregloBotones.add(btn_2_0);

		JButton btn_2_1 = new JButton();
		btn_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(2, 1);
				actualizarTablero(arregloBotones);
			}
		});
		btn_2_1.setBounds(223, 178, 50, 50);
		frame.getContentPane().add(btn_2_1);
		arregloBotones.add(btn_2_1);

		JButton btn_2_2 = new JButton();
		btn_2_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(2, 2);
				actualizarTablero(arregloBotones);
			}
		});
		btn_2_2.setBounds(283, 178, 50, 50);
		frame.getContentPane().add(btn_2_2);
		arregloBotones.add(btn_2_2);

		JButton btn_2_3 = new JButton();
		btn_2_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(2, 3);
				actualizarTablero(arregloBotones);
			}
		});
		btn_2_3.setBounds(341, 178, 50, 50);
		frame.getContentPane().add(btn_2_3);
		arregloBotones.add(btn_2_3);

		JButton btn_3_0 = new JButton();
		btn_3_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(3, 0);
				actualizarTablero(arregloBotones);
			}
		});
		btn_3_0.setBounds(165, 234, 50, 50);
		frame.getContentPane().add(btn_3_0);
		arregloBotones.add(btn_3_0);

		JButton btn_3_1 = new JButton();
		btn_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(3, 1);
				actualizarTablero(arregloBotones);
			}
		});
		btn_3_1.setBounds(223, 234, 50, 50);
		frame.getContentPane().add(btn_3_1);
		arregloBotones.add(btn_3_1);

		JButton btn_3_2 = new JButton();
		btn_3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(3, 2);
				actualizarTablero(arregloBotones);
			}
		});
		btn_3_2.setBounds(283, 234, 50, 50);
		frame.getContentPane().add(btn_3_2);
		arregloBotones.add(btn_3_2);

		JButton btn_3_3 = new JButton();
		btn_3_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tab.cambiarEstado(3, 3);
				actualizarTablero(arregloBotones);
			}
		});
		btn_3_3.setBounds(341, 234, 50, 50);
		frame.getContentPane().add(btn_3_3);
		arregloBotones.add(btn_3_3);
	

		//Actualiza el tablero
		actualizarTablero(arregloBotones);


	}

	protected void actualizarTablero(ArrayList<JButton> botones) {
		int indice=0;
		for(int i = 0; i < tab.matriz.length; i++) {
			for(int j = 0; j < tab.matriz[i].length; j++) {
				indice++;
				if(tab.matriz[i][j]) {
					botones.get(indice-1).setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
					botones.get(indice-1).setBackground(Color.black);
					botones.get(indice-1).setBorderPainted(false);
					
				}
				else {
					botones.get(indice-1).setBorderPainted(true);
					botones.get(indice-1).setBackground(Color.BLACK);
					botones.get(indice-1).setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
				}
			}
		}
	}

}
